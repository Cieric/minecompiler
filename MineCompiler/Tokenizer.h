#pragma once
#include <vector>
#include "Token.h"
#include "Keywords.h"

class Tokenizer
{
	std::string source;

	Position start;
	Position current;

	std::vector<Token> tokens;
	bool isAtEnd();
	char advance();
	void addToken(TokenType type);
	void addToken(TokenType type, Literal value);
	bool match(char expected);
	char peek();
	char peekNext();
	void parseString();
	void parseNumber();
	void parseIdentifier();
	bool isDigit(char c);
	bool isAlpha(char c);
	bool isAlphaNumeric(char c);
	void scanToken();

public:
	std::vector<Token> tokenize(std::string source);

};