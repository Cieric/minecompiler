#pragma once
#include <vector>
#include "Token.h"
#include "Expr.h"
#include "Stmt.h"

class Parser
{
	size_t current = 0;
	std::vector<Token> tokens;

	bool isAtEnd();

	Token peek();

	Token previous();

	Token advance();

	bool check(TokenType type);

	bool match(TokenType types);
	bool match(std::vector<TokenType> types);

	Token consume(TokenType type, std::string message);

	Expr* expression();
	Stmt* declaration();
	Stmt* statement();
	Stmt* expressionStatement();
	Expr* assignment();
	std::vector<Stmt*> block();
	Stmt* printStatement();
	Stmt* varDeclaration();
	Expr* equality();
	Expr* comparison();
	Expr* addition();
	Expr* multiplication();
	Expr* unary();
	Expr* finishCall(Expr* callee);
	Expr* call();
	Expr* primary();

public:
	std::vector<Stmt*>  parse(std::vector<Token> tokens);
	Parser();

};