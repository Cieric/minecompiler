#pragma once
#include <stdint.h>

struct Position
{
	uint32_t line = 1;
	uint32_t column = 1;
	uint32_t index = 0;
public:
	void newline();

	Position& operator++(int);

	Position& operator=(const Position& lhs);
};