#include "Parser.h"
#include "ParseError.h"
#include "Expr.h"
#include "Stmt.h"

bool Parser::isAtEnd() {
	return peek().type == TokenType::EOFile;
}

Token Parser::peek() {
	return tokens[current];
}

Token Parser::previous()
{
	return tokens[current - 1];
}

Token Parser::advance()
{
	if (!isAtEnd()) current++;
	return previous();
}

bool Parser::check(TokenType type)
{
	if (isAtEnd()) return false;
	return peek().type == type;
}

bool Parser::match(TokenType type)
{
	if (check(type)) {
		(void)advance();
		return true;
	}
	return false;
}

bool Parser::match(std::vector<TokenType> types)
{
	for (TokenType type : types) {
		if (check(type)) {
			(void)advance();
			return true;
		}
	}
	return false;
}

Token Parser::consume(TokenType type, std::string message)
{
	if (check(type)) return advance();
	throw new ParseError(message.c_str());
}

Expr* Parser::expression()
{
	return assignment();
}

Stmt* Parser::declaration() {
	try {
		if (match({ TokenType::VAR })) return varDeclaration();

		return statement();
	}
	catch (ParseError error) {
		//synchronize();
		return nullptr;
	}
}

Stmt* Parser::statement()
{
	if (match({ TokenType::PRINT })) return printStatement();
	if (match({ TokenType::LEFT_BRACE })) return new StmtBlock(block());

	return expressionStatement();
}

Stmt* Parser::expressionStatement() {
	Expr* expr = expression();
	(void)consume({ TokenType::SEMICOLON }, "Expect ';' after expression.");
	return new StmtExpression(expr);
}

Expr* Parser::assignment() {
	Expr* expr = equality();

	if (match(TokenType::EQUAL)) {
		Token equals = previous();
		Expr* value = assignment();

		if (expr->instanceOf(ExprType::Variable)) {
			Token name = ((ExprVariable*)expr)->getName();
			return new ExprAssign(name, value);
		}

		throw new ParseError("Invalid assignment target.");
	}

	return expr;
}

std::vector<Stmt*> Parser::block() {
	std::vector<Stmt*> statements;

	while (!check({ TokenType::RIGHT_BRACE }) && !isAtEnd()) {
		statements.push_back(declaration());
	}

	(void)consume({ TokenType::RIGHT_BRACE }, "Expect '}' after block.");
	return statements;
}

Stmt* Parser::printStatement() {
	Expr* value = expression();
	(void)consume({ TokenType::SEMICOLON }, "Expect ';' after value.");
	return new StmtPrint(value);
}

Stmt* Parser::varDeclaration() {
	Token name = consume({ TokenType::IDENTIFIER }, "Expect variable name.");

	Expr* initializer = nullptr;
	if (match({ TokenType::EQUAL })) {
		initializer = expression();
	}

	(void)consume({ TokenType::SEMICOLON }, "Expect ';' after variable declaration.");
	return new StmtVar(name, initializer);
}

Expr* Parser::equality()
{
	Expr* expr = comparison();

	while (match({ TokenType::BANG_EQUAL, TokenType::EQUAL_EQUAL })) {
		Token op = previous();
		Expr* right = comparison();
		expr = new ExprBinary(expr, op, right);
	}

	return expr;
}

Expr* Parser::comparison()
{
	Expr* expr = addition();

	while (match({ TokenType::GREATER, TokenType::GREATER_EQUAL, TokenType::LESS, TokenType::LESS_EQUAL })) {
		Token op = previous();
		Expr* right = addition();
		expr = new ExprBinary(expr, op, right);
	}

	return expr;
}

Expr* Parser::addition()
{
	Expr* expr = multiplication();

	while (match({ TokenType::MINUS, TokenType::PLUS })) {
		Token op = previous();
		Expr* right = multiplication();
		expr = new ExprBinary(expr, op, right);
	}

	return expr;
}

Expr* Parser::multiplication()
{
	Expr* expr = unary();

	while (match({ TokenType::SLASH, TokenType::STAR })) {
		Token op = previous();
		Expr* right = unary();
		expr = new ExprBinary(expr, op, right);
	}

	return expr;
}

Expr* Parser::unary()
{
	if (match({ TokenType::BANG, TokenType::MINUS })) {
		Token op = previous();
		Expr* right = unary();
		return new ExprUnary(op, right);
	}

	return call();
}

Expr* Parser::finishCall(Expr* callee) {
	std::vector<Expr*> arguments;
	if (!check({ TokenType::RIGHT_PAREN })) {
		do {
			arguments.push_back(expression());
		} while (match({ TokenType::COMMA }));
	}

	Token paren = consume({ TokenType::RIGHT_PAREN }, "Expect ')' after arguments.");

	return new ExprCall(callee, arguments);
}

Expr* Parser::call() {
	Expr* expr = primary();

	while (true) {
		if (match({ TokenType::LEFT_PAREN })) {
			expr = finishCall(expr);
		}
		else {
			break;
		}
	}

	return expr;
}

Expr* Parser::primary()
{
	if (match(TokenType::FALSE)) return new ExprLiteral(false);
	if (match(TokenType::TRUE)) return new ExprLiteral(true);
	if (match(TokenType::NIL)) return new ExprLiteral(nullptr);

	if (match({ TokenType::NUMBER, TokenType::STRING })) {
		return new ExprLiteral(previous().value);
	}

	if (match(TokenType::IDENTIFIER)) {
		return new ExprVariable(previous());
	}

	if (match(TokenType::LEFT_PAREN)) {
		Expr* expr = expression();
		(void)consume(TokenType::RIGHT_PAREN, "Expect ')' after expression.");
		return new ExprGrouping(expr);
	}

	throw new ParseError("Expression Expected.");
}

std::vector<Stmt*> Parser::parse(std::vector<Token> tokens)
{
	this->tokens = tokens;
	std::vector<Stmt*> statements;
	while (!isAtEnd()) {
		statements.push_back(declaration());
	}
	return statements;
}

Parser::Parser()
{
}
