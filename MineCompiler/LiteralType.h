#pragma once

enum class LiteralType
{
	Void,
	String,
	Integer,
	Float,
};