#pragma once
#include <stdexcept>

class ParseError : public std::exception
{

public:
	ParseError(const char* message);

};