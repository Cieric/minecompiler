#include "Position.h"

void Position::newline()
{
	line++;
	column = 1;
}

Position& Position::operator++(int)
{
	column++;
	index++;
	return *this;
}

Position& Position::operator=(const Position& lhs)
{
	this->column = lhs.column;
	this->line = lhs.line;
	this->index = lhs.index;
	return *this;
}
