#include "Literal.h"

Literal::Literal(void* value, LiteralType type) :
	type(type),
	value(value)
{}

Literal::Literal(std::string value, LiteralType type) :
	type(type),
	value(new std::string(value))
{}

Literal::Literal(int value, LiteralType type) :
	type(type),
	value(new int(value))
{}

Literal::Literal(float value, LiteralType type) :
	type(type),
	value(new float(value))
{}

std::string Literal::toString() const
{
	switch (type) {
	case LiteralType::Void:
		return "";
	case LiteralType::String:
		return "\"" + *(std::string*)value + "\"";
	case LiteralType::Integer:
		return std::to_string(*(int*)value);
	case LiteralType::Float:
		return std::to_string(*(float*)value);
	default:
		return "unknown";
	}
};
