#pragma once
#include <string>
#include "LiteralType.h"

class Literal
{
	LiteralType type;
	void* value;

public:
	Literal(void* value, LiteralType type = LiteralType::Void);
	Literal(std::string value, LiteralType type = LiteralType::String);
	Literal(int value, LiteralType type = LiteralType::Integer);
	Literal(float value, LiteralType type = LiteralType::Float);

	std::string toString() const;
};