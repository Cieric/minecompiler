#include "Tokenizer.h"

bool Tokenizer::isAtEnd()
{
	return current.index >= source.length();
}

char Tokenizer::advance()
{
	if (isAtEnd()) return '\0';
	current++;
	return source[current.index - 1];
}

void Tokenizer::addToken(TokenType type)
{
	addToken(type, Literal(nullptr));
}

void Tokenizer::addToken(TokenType type, Literal value)
{
	std::string lexeme = source.substr(start.index, current.index - start.index);
	tokens.push_back(Token(type, lexeme, value, start));
	start = current;
}

bool Tokenizer::match(char expected)
{
	if (isAtEnd()) return false;
	if (source[current.index] != expected) return false;

	current++;
	return true;
}

char Tokenizer::peek()
{
	if (isAtEnd()) return '\0';
	return source[current.index];
}

char Tokenizer::peekNext()
{
	if (current.index + 1 >= source.length()) return '\0';
	return source[current.index + 1];
}

void Tokenizer::parseString()
{
	while (peek() != '"' && !isAtEnd()) {
		if (peek() == '\n') current.newline();
		advance();
	}

	// Unterminated string.                                 
	if (isAtEnd()) {
		throw "Unterminated string.";
		return;
	}

	// The closing ".                                       
	advance();

	// Trim the surrounding quotes.                         
	std::string value = source.substr(start.index + 1, current.index - 1);
	addToken(TokenType::STRING, value);
}

void Tokenizer::parseNumber()
{
	while (isDigit(peek())) advance();

	bool floatingPoint = false;

	// Look for a fractional part.                            
	if (peek() == '.' && isDigit(peekNext())) {
		floatingPoint = true;
		// Consume the "."                                      
		advance();

		while (isDigit(peek())) advance();
	}
	if(floatingPoint)
		addToken(TokenType::NUMBER, stof(source.substr(start.index, current.index)));
	else
		addToken(TokenType::NUMBER, stoi(source.substr(start.index, current.index)));
}

void Tokenizer::parseIdentifier()
{
	while (isAlphaNumeric(peek())) advance();
	std::string text = source.substr(start.index, current.index);

	if (keywords.find(text) != keywords.end())
	{
		TokenType type = keywords[text];
		addToken(type);
	}
	else
	{
		addToken(TokenType::IDENTIFIER);
	}
}

bool Tokenizer::isDigit(char c)
{
	return c >= '0' && c <= '9';
}

bool Tokenizer::isAlpha(char c)
{
	return (c >= 'a' && c <= 'z') ||
		(c >= 'A' && c <= 'Z') ||
		c == '_';
}

bool Tokenizer::isAlphaNumeric(char c)
{
	return isAlpha(c) || isDigit(c);
}

void Tokenizer::scanToken()
{
	char c = advance();
	switch (c)
	{
	case '(': addToken(TokenType::LEFT_PAREN); break;
	case ')': addToken(TokenType::RIGHT_PAREN); break;
	case '[': addToken(TokenType::LEFT_BRACE); break;
	case ']': addToken(TokenType::RIGHT_BRACE); break;
	case '{': addToken(TokenType::LEFT_CURLY); break;
	case '}': addToken(TokenType::RIGHT_CURLY); break;
	case ',': addToken(TokenType::COMMA); break;
	case '.': addToken(TokenType::DOT); break;
	case '-': addToken(TokenType::MINUS); break;
	case '+': addToken(TokenType::PLUS); break;
	case ';': addToken(TokenType::SEMICOLON); break;
	case '*': addToken(TokenType::STAR); break;
	case '<': addToken(match('=') ? TokenType::LESS_EQUAL : TokenType::LESS); break;
	case '>': addToken(match('=') ? TokenType::GREATER_EQUAL : TokenType::GREATER); break;

	case '!': addToken(match('=') ? TokenType::BANG_EQUAL : TokenType::BANG); break;
	case '=': addToken(match('=') ? TokenType::EQUAL_EQUAL : TokenType::EQUAL); break;

	case '"': parseString(); break;

	case '/': {
		if (match('/')) {
			while (peek() != '\n' && !isAtEnd()) advance();
		}
		else if (isAlpha(c)) {
			parseIdentifier();
		}
		else {
			addToken(TokenType::SLASH);
		}
	} break;

	case ' ':
	case '\t':
	case '\0':
		break;

	case '\r':
		match('\n');
	case '\n':
		current.newline();
		break;

	default:
		if (isDigit(c)) {
			parseNumber();
		}
		else if (isAlpha(c)) {
			parseIdentifier();
		}
		else {
			throw "Unexpected character.";
		}
		break;
	}
}

std::vector<Token> Tokenizer::tokenize(std::string source)
{
	this->source = source;
	tokens.clear();

	while (!isAtEnd()) {
		start = current;
		scanToken();
	}

	start = current;
	addToken(TokenType::EOFile);
	return tokens;
}
