#include "ParseError.h"

ParseError::ParseError(const char* message) :
	std::exception(message)
{}
