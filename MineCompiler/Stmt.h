#pragma once
#include <string>
#include "Token.h"
#include "Literal.h"

class Expr;

enum class StmtType
{
	Var,
	Print,
	Expression,
	Block,
};

class Stmt
{
	StmtType type;
public:
	Stmt(StmtType type);

	virtual std::string toString() = 0;
};

class StmtVar : public Stmt
{
	Token name;
	Expr* initializer;
public:
	StmtVar(Token name, Expr* initializer);

	std::string toString();
};

class StmtPrint : public Stmt
{
	Expr* value;
public:
	StmtPrint(Expr* value);

	std::string toString();
};

class StmtExpression : public Stmt
{
	Expr* value;
public:
	StmtExpression(Expr* value);

	std::string toString();
};

class StmtBlock : public Stmt
{
	std::vector<Stmt*> stmts;
public:
	StmtBlock(std::vector<Stmt*> stmts);

	std::string toString();
};