#include <iostream>
#include <fstream>
#include <streambuf>
#include "Token.h"
#include "Tokenizer.h"
#include "Parser.h"

int main(int argc, char** argv)
{
	std::ifstream file("main.mcp");
	std::string source((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

	Tokenizer tokenizer;
	std::vector<Token> tokens = tokenizer.tokenize(source);

	//for (Token tok : tokens)
	//	printf("%s\n", tok.toString().c_str());

	Parser parser;
	std::vector<Stmt*> stmts = parser.parse(tokens);

	for(Stmt* stmt : stmts)
		std::cout << stmt->toString() << std::endl;
}