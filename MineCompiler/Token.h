#pragma once
#include <string>
#include <sstream>
#include <unordered_map>
#include "TokenType.h"
#include "Literal.h"
#include "Position.h"


class Token {
public:
	TokenType type;
	std::string lexeme;
	Literal value;
	Position position;

	Token(TokenType type, std::string lexeme, Literal value, Position position);

	std::string toString() const;
};