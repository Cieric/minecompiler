#pragma once
#include <string>

class StringBuilder {
private:
	std::string main;
	std::string scratch;

	const std::string::size_type ScratchSize = 1024;  // or some other arbitrary number

public:
	StringBuilder& append(const std::string& str);

	const std::string& str();
};