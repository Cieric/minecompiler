#include "Stmt.h"
#include "Expr.h"

Stmt::Stmt(StmtType type) :
	type(type)
{
}

StmtVar::StmtVar(Token name, Expr* initializer) : 
	Stmt(StmtType::Var),
	name(name),
	initializer(initializer)
{
}

std::string StmtVar::toString()
{
	return name.toString() + "(" + initializer->toString() + ")";
}

StmtPrint::StmtPrint(Expr* value) :
	Stmt(StmtType::Print),
	value(value)
{
}

std::string StmtPrint::toString()
{
	return "print(" + value->toString() + ")";
}

StmtExpression::StmtExpression(Expr* value) :
	Stmt(StmtType::Expression),
	value(value)
{

}

std::string StmtExpression::toString()
{
	return value->toString();
}

StmtBlock::StmtBlock(std::vector<Stmt*> stmts) :
	Stmt(StmtType::Block),
	stmts(stmts)
{
}

std::string StmtBlock::toString()
{
	std::string strs;

	for (Stmt* stmt : stmts)
		strs += stmt->toString() + ";\r\n";

	return "{\r\n" + strs + "}";
}
