#include "Token.h"

Token::Token(TokenType type, std::string lexeme, Literal value, Position position) :
	type(type),
	lexeme(lexeme),
	value(value),
	position(position)
{}

std::string Token::toString() const
{
	std::stringstream builder;
	builder << "[" << position.line << "," << position.column << "] " << (int)type << " " << lexeme << " " << value.toString();
	return builder.str();
}
