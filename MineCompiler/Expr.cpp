#include "Expr.h"

Expr::Expr(ExprType type) :
	type(type)
{
}

bool Expr::instanceOf(ExprType otherType)
{
	return type == otherType;
}

ExprBinary::ExprBinary(Expr* left, Token op, Expr* right) :
	Expr(ExprType::Binary),
	left(left),
	op(op),
	right(right)
{
}

std::string ExprBinary::toString()
{
	return op.lexeme + " " + left->toString() + " " + right->toString();
}

ExprUnary::ExprUnary(Token op, Expr* right) :
	Expr(ExprType::Unary),
	op(op),
	right(right)
{
}

std::string ExprUnary::toString()
{
	return op.lexeme + right->toString();
}

ExprCall::ExprCall(Expr* callee, std::vector<Expr*> arguments) :
	Expr(ExprType::Call),
	callee(callee),
	arguments(arguments)
{
}

std::string ExprCall::toString()
{
	std::string args = "";
	bool first = true;
	for (Expr* argument : arguments)
	{
		if (first)
		{
			args += "( ";
			first = false;
		}
		else
		{
			args += ", ";
		}

		args += argument->toString();
	}
	return callee->toString() + args + " )";
}

ExprLiteral::ExprLiteral(Literal lit) :
	Expr(ExprType::Literal),
	lit(lit)
{
}

std::string ExprLiteral::toString()
{
	return this->lit.toString();
}

ExprGrouping::ExprGrouping(Expr* expr) :
	Expr(ExprType::Grouping),
	expr(expr)
{
}

std::string ExprGrouping::toString()
{
	return expr->toString();
}

ExprVariable::ExprVariable(Token name) :
	Expr(ExprType::Variable),
	name(name)
{
}

Token ExprVariable::getName()
{
	return name;
}

std::string ExprVariable::toString()
{
	return name.lexeme;
}

ExprAssign::ExprAssign(Token name, Expr* value) :
	Expr(ExprType::Assign),
	name(name),
	value(value)
{
}

std::string ExprAssign::toString()
{
	return name.lexeme + " = " + value->toString() + ";";
}
