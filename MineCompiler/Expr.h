#pragma once
#include <string>
#include "Token.h"
#include "Literal.h"

enum class ExprType
{
	Binary,
	Unary,
	Literal,
	Grouping,
	Call,
	Variable,
	Assign,
};

class Expr
{
	ExprType type;
public:
	Expr(ExprType type);

	bool instanceOf(ExprType otherType);

	virtual std::string toString() = 0;
};

class ExprBinary : public Expr
{
	Expr* left;
	Expr* right;
	Token op;
public:
	ExprBinary(Expr* left, Token op, Expr* right);
	std::string toString();
};

class ExprUnary : public Expr
{
	Expr* right;
	Token op;
public:
	ExprUnary(Token op, Expr* right);
	std::string toString();
};

class ExprCall : public Expr
{
	Expr* callee;
	std::vector<Expr*> arguments;
public:
	ExprCall(Expr* callee, std::vector<Expr*> arguments);
	std::string toString();
};

class ExprLiteral : public Expr
{
	Literal lit;
public:
	ExprLiteral(Literal lit);
	std::string toString();
};

class ExprGrouping : public Expr
{
	Expr* expr;
public:
	ExprGrouping(Expr* expr);
	std::string toString();
};

class ExprVariable : public Expr
{
	Token name;
public:
	ExprVariable(Token name);

	Token getName();

	std::string toString();
};

class ExprAssign : public Expr
{
	Token name;
	Expr* value;
public:
	ExprAssign(Token name, Expr* value);

	std::string toString();
};